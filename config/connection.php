<?php

//connection
function OpenCon(){

    $servername = "localhost";
    $dbusername = "root";
    $password = "";
    $dbname = "tasks";

    $conn = new mysqli($servername, $dbusername, $password,$dbname);
    if($conn->connect_error){
        die("Connection failed: " . $conn->connect_error);
    }else{
        return $conn;
    }

}

//close connection
function CloseCon($conn){

    $conn -> close();
    
}

?>
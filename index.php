<?php

require 'flight/Flight.php';
include 'config/connection.php';

//register
Flight::route('GET /register ', function () {

    Flight::render('register.php');

});

//check mail already exists or not
Flight::route('POST /check-mail', function () {

    $conn = OpenCon();
    $email = mysqli_real_escape_string($conn,$_REQUEST['email']);

    $queries = "select * from `users`";
    if(isset($email) && $email != '' && !isset($psw)){
        $queries .= " where `email` = '".$email."'";	
    }

    $result = mysqli_query($conn,$queries);

    $data=mysqli_fetch_assoc($result);

    if (isset($data) && !empty($data) ) {
        echo 'false';
    }else{
        echo 'true';
    }
    
    CloseCon($conn);

});

//save register data
Flight::route('POST /post-register ', function () {

    $conn = OpenCon();

    $name = mysqli_real_escape_string($conn,$_POST['name']);
    $email = mysqli_real_escape_string($conn,$_POST['email']);
    $password = mysqli_real_escape_string($conn,$_POST['psw']);
    $phoneNumber = mysqli_real_escape_string($conn,$_POST['phone_number']);

    //hash conversion
    $hash_val = md5($password);

    $sql = "INSERT INTO users (name, email, password, phone_number)
            VALUES ('$name','$email','$hash_val', '$phoneNumber')";
    
    $result = $conn->query($sql);

    if($result == 1){
        echo 'true';
    }else{
        echo 'false';
    }

    //send confirmation mail
    $link = 'localhost/task/confirm-mail';
    $to_email = $_POST['email'];
    $subject = 'Conformation Mail';
    $message = "This is autogenrated mail. Please confirm your mail by using this link'" . $link.'/'.$to_email. "'";
    $headers = 'From: davedarshit1@gmail.com';
    mail($to_email,$subject,$message,$headers);

    CloseCon($conn);

    Flight::render('login.php');
    
});

//confirm mail
Flight::route('GET /confirm-mail/@email ', function ($email) {
    
    $conn = OpenCon();

    $query = "UPDATE users set is_confirm='1' WHERE email='" . $email . "'";

    $query_run = mysqli_query($conn,$query);
    
    CloseCon($conn);
    
    Flight::render('confirmMail.php');

});

//login
Flight::route('GET /login ', function () {

    Flight::render('login.php');

});

//check login credentials
Flight::route('POST /check-credentials ', function () {
    
    $conn = OpenCon();

    $email = mysqli_real_escape_string($conn,$_REQUEST['email']);
    if(isset($_REQUEST['psw'])){
        $psw = mysqli_real_escape_string($conn,$_REQUEST['psw']);	

        //hash conversion
        $hash_val = md5($psw);
    }
    
    $queries = "select * from `users`";
    if(isset($email) && isset($psw)){
        $queries .= " where `password` = '".$hash_val."' and `email` = '".$email."' and is_confirm='1'";	
    }
    
    $result = mysqli_query($conn,$queries);
    
    $data=mysqli_fetch_assoc($result);

    if (isset($data) && !empty($data) ) {
        echo 'true';
    }else{
        echo 'false';
    }
    
    CloseCon($conn);

});

//authenticate user
Flight::route('POST /post-login ', function () {
    
    $conn = OpenCon();

    session_start();
    $email = mysqli_real_escape_string($conn,$_POST['email']);
    $password = mysqli_real_escape_string($conn,$_POST['psw']);

    //hash conversion
    $hash_val = md5($password);

    $query = "SELECT * FROM users WHERE email='$email' AND password='$hash_val' AND is_confirm='1'";
    
    $query_run = mysqli_query($conn,$query);
	
	if(mysqli_num_rows($query_run) >= 1){
        
        $qry = mysqli_fetch_assoc($query_run);
        //stor in session
        $_SESSION['email'] = $qry['email'];
        
    }

    CloseCon($conn);

    $_SESSION['login_success'] = 1;

    Flight::redirect('/profile-details');

});

//get profile details
Flight::route('GET /profile-details ', function () {

    session_start();
    if(!isset($_SESSION['email'])){

        Flight::redirect('/login');

    }else{

        $email = $_SESSION['email'];

        $conn = OpenCon();

        $queries = "SELECT * FROM users WHERE email = '$email' ";
        
        $query_run = mysqli_query($conn,$queries);

        if(!$query_run) {
            var_dump(mysqli_error($conn));
            exit;
        }

        $query_rows = mysqli_fetch_assoc($query_run);

        CloseCon($conn);

        Flight::render('profile.php', array('profile_image' => $query_rows['profile_image'],'name' => $query_rows['name'], 'phoneNumber' => $query_rows['phone_number']));
    }
    

});

//update profile
Flight::route('POST /update-profile', function () {
        
    session_start();

    if(!isset($_SESSION['email'])){

        Flight::redirect('/login');

    }else{
    
        $conn = OpenCon();

        $email = $_SESSION['email'];
        
        if($_FILES['profile_image']["name"] != ''){
            $image = $_FILES["profile_image"]["name"];
            $tm = $_FILES["profile_image"]["tmp_name"];
            $location = 'public/assets/images/profile/';
            move_uploaded_file($tm, $location.$image);
        }
        
        if($_FILES['profile_image']["name"] != ''){
            $profile_image = mysqli_real_escape_string($conn,$image);
        }else{
            $profile_image = $_POST['profile_image'];
        }

        $query = "UPDATE users set name='" . $_POST['name'] . "',profile_image='" . $profile_image . "', phone_number='" . $_POST['phone_number'] . "' WHERE email='" . $email . "'";
        
        $query_run = mysqli_query($conn,$query);
        
        CloseCon($conn);
        
        $_SESSION['profile_updated'] = 1;

        Flight::redirect('/profile-details');
    
    }

});

//logout
Flight::route('GET /logout ', function () {

    //destroy sesssion
    session_start();
    unset($_SESSION['email']);

    session_destroy();

    Flight::redirect('/login');
    
});

Flight::start();



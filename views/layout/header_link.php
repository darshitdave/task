<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keywords" content="">

    <!-- Title Page-->
    <title></title>

    <!-- Bootstrap css -->
    <link href="/task/public/assets/plugins/bootstrap/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- custom css -->
    <link href="/task/public/assets/css/developer.css" rel="stylesheet" type="text/css" />
    <!-- toastr -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css"/>
    <!-- jquery -->
    <script src="/task/public/assets/js/jquery.min.js"></script>
    <!-- dropify -->
    <link href="/task/public/assets/plugins/dropify/dist/css/dropify.css" rel="stylesheet" type="text/css" />

</head>
    <body>
<?php include 'layout/header_link.php';?>

<form class="custom-validation" action="/task/post-register" method="POST" id="registerForm">
  <div class="container">
    <h1>Register</h1>
    <p>Please fill in this form to create an account.</p>
    <hr>

    <div class="form-group">
      <label for="name"><b>Name</b></label>
      <input type="text" class="form-control" placeholder="Enter Name" name="name" id="name" required>
    </div>

    <div class="form-group">
      <label for="email"><b>Email</b></label>
      <input type="text" placeholder="Enter Email" class="form-control email" name="email" id="email" required>
    </div>

    <div class="form-group">
      <label for="psw"><b>Password</b></label>
      <input type="password" placeholder="Enter Password" class="form-control" name="psw" id="psw" required>
    </div>

    <div class="form-group">
      <label for="psw-repeat"><b>Confirm Password</b></label>
      <input type="password" placeholder="Repeat Password" class="form-control confirm_password" name="confirm_password" id="psw-repeat" required>
    </div>

    <div class="form-group">
      <label for="phoneNumber"><b>Phone Number</b></label>
      <input type="text" placeholder="Enter Phone Number" name="phone_number" class="form-control" id="phone_number" maxlength="10" pattern="^\d{10}$" oninvalid="this.setCustomValidity('Please Enter valid Mobile Number')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" required>
    </div>

    <hr>
    <div class="form-group">
      <center>
      <button type="submit" class="btn btn-primary btn-lg">Register</button>
      </center>
    </div>
  </div>
  
  <div class="container signin">
    <p>Already have an account? <a href="/task/login">Sign in</a>.</p>
  </div>
</form>

<?php include 'layout/footer_link.php';?>

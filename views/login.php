
<?php include 'layout/header_link.php';?>

<form action="/task/post-login" method="POST" id="loginForm">
  <div class="container">
    <h1>Login</h1>
    <!-- <p></p> -->
    <hr>
    <div class="form-group">
      <label for="email"><b>Email</b></label>
      <input type="text" placeholder="Enter Email" class="form-control email" name="email" id="email" required>
    </div>

    <div class="form-group">
      <label for="psw"><b>Password</b></label>
      <input type="password" placeholder="Enter Password" name="psw" class="form-control psw" id="psw" required>
    </div>

    <div class="form-group">
      <center>
      <button type="submit" class="btn btn-primary btn-lg">Login</button>
      </center>
    </div>

  </div>

  <div class="container signin">
    <p>Not have account? <a href="/task/register">Join now</a>.</p>
  </div>
</form>

<?php include 'layout/footer_link.php';?>



<?php include 'layout/header_link.php';?>

<form action="/task/update-profile" method="POST" id="updateProfileForm" enctype="multipart/form-data">
  <div class="container">
    <a href="/task/logout" type="button" class="btn btn-danger" style="float: right;">Logout</a>
    <h1>Profile Details</h1>
    
    <!-- <p></p> -->
    <hr>

    <div class="form-group">
        <label for="profile_image">Profile Image</label>
        <input type="file" name="profile_image" class="dropify" data-allowed-file-extensions="png jpg"  data-default-file="/task/public/assets/images/profile/<?php echo $profile_image ?>"  value="<?php echo $profile_image ?>">
        <input type="hidden" name="profile_image" value="<?php echo $profile_image ?>">
        
    </div>

    <div class="form-group">
    <label for="email"><b>Name</b></label>
      <input type="text" placeholder="Enter Name" name="name" class="form-control" id="name" value="<?php echo $name; ?>" required>
    </div>

    <div class="form-group">
    <label for="phoneNumber"><b>Phone Number</b></label>
      <input type="text" placeholder="Enter Phone Number" name="phone_number" class="form-control" id="phone_number" value="<?php echo $phoneNumber; ?>" maxlength="10" pattern="^\d{10}$" oninvalid="this.setCustomValidity('Please Enter valid Mobile Number')" oninput="setCustomValidity('')" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" required>
    </div>

    <div class="form-group">
      <center>
      <button type="submit" class="btn btn-primary btn-lg">Update Profile</button>
      </center>
    </div>
  </div>
</form>

<?php include 'layout/footer_link.php';?>

<script type="text/javascript">
    <?php if(isset($_SESSION['profile_updated']) == 1){ ?>
        toastr.success("Profile succefully change!");
        
    <?php }
    unset($_SESSION['profile_updated']);
     ?>

    <?php if(isset($_SESSION['login_success']) == 1){ ?>
        toastr.success("Login succefully!");
        
    <?php }
    unset($_SESSION['login_success']);
     ?>

    $(document).ready(function() {
        $('.dropify').dropify();
    });
    
</script>
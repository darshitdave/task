$(document).on('change','.faq-switch',function(){
    if(this.checked){
        option = 1;
    } else {
        option = 0;
    }

    var type = 1;
    var id = $(this).data('id');
    var value = $(this).data('value');

    var dataString = 'id=' + id + '&option=' + option + '&type=' + type + '&value=' + value;

    $.ajax({
        url: "status_update.php",
        type: "POST",
        data : dataString,
        success: function(data){
            if(data == 'true'){
                if(option == 1){
                    toastr.success('Category Status Successfully Activated!');    
                }else if(option == 0){
                    toastr.success('Category Status Successfully Deactivated!');    
                }
                
            }else{
                toastr.success('Category Status Successfully Change!');
            }
        }
    });
})

$(document).on('change','.job-switch',function(){
    if(this.checked){
        option = 1;
    } else {
        option = 0;
    }

    var type = 3;
    var id = $(this).data('id');
    var value = $(this).data('value');

    var dataString = 'id=' + id + '&option=' + option + '&type=' + type + '&value=' + value;

    $.ajax({
        url: "status_update.php",
        type: "POST",
        data : dataString,
        success: function(data){

            if(data == 'true'){
                if(option == 1){
                    toastr.success('Job Application Status Successfully Activated!');
                }else if(option == 0){
                    toastr.success('Job Application Status Successfully Deactivated!');
                }
                
            }else{
                toastr.success('Category Status Successfully Change!');
            }
        }
    });
})

$(document).on('change','.contact-switch',function(){
    if(this.checked){
        option = 1;
    } else {
        option = 0;
    }

    var type = 2;
    var id = $(this).data('id');
    var value = $(this).data('value');

    var dataString = 'id=' + id + '&option=' + option + '&type=' + type + '&value=' + value;

    $.ajax({
        url: "status_update.php",
        type: "POST",
        data : dataString,
        success: function(data){

            if(data == 'true'){
                if(option == 1){
                    toastr.success('Status Successfully Activated!');
                }else if(option == 0){
                    toastr.success('Status Successfully Deactivated!');
                }
            }else{
                toastr.success('Status Successfully Change!');
            }
        }
    });
})


$(document).on('change','.service-provider-switch',function(){
    if(this.checked){
        option = 1;
    } else {
        option = 0;
    }

    var type = 4;
    var id = $(this).data('id');
    var value = $(this).data('value');

    var dataString = 'id=' + id + '&option=' + option + '&type=' + type + '&value=' + value;

    $.ajax({
        url: "status_update.php",
        type: "POST",
        data : dataString,
        success: function(data){

            if(data == 'true'){
                if(option == 1){
                    toastr.success('Status Successfully Activated!');
                }else if(option == 0){
                    toastr.success('Status Successfully Deactivated!');
                }
            }else{
                toastr.success('Status Successfully Change!');
            }
        }
    });
})

$(document).on('change','.ppa',function(){
    
    if(this.checked){
        option = 1;
    } else {
        option = 0;
    }

    var type = 5;
    var id = $(this).data('id');
    var value = $(this).data('value');

    var dataString = 'id=' + id + '&option=' + option + '&value=' + value+ '&type=' + type;
    
    $.ajax({
        url: "status_update.php",
        type: "POST",
        data : dataString,
        success: function(data){
            if(data == 'true'){
                if(option == 1){
                    toastr.success('Status Successfully Activated!');
                }else if(option == 0){
                    toastr.success('Status Successfully Deactivated!');
                }
            }else{
                toastr.success('Status Successfully Change!');
            }
        }
    });
})

$(document).on('change','.spa',function(){

    if(this.checked){
        option = 1;
    } else {
        option = 0;
    }

    var type = 6;
    var id = $(this).data('id');
    var value = $(this).data('value');

    var dataString = 'id=' + id + '&option=' + option + '&value=' + value+ '&type=' + type;

    $.ajax({
        url: "status_update.php",
        type: "POST",
        data : dataString,
        success: function(data){
            
            if(data == 'true'){
                if(option == 1){
                    toastr.success('Status Successfully Activated!');
                }else if(option == 0){
                    toastr.success('Status Successfully Deactivated!');
                }
            }else{
                toastr.success('Status Successfully Change!');
            }
        }
    });
})

$(document).on('change','.faq-switch',function(){
    if(this.checked){
        option = 1;
    } else {
        option = 0;
    }

    var type = 1;
    var id = $(this).data('id');
    var value = $(this).data('value');

    var dataString = 'id=' + id + '&option=' + option + '&type=' + type + '&value=' + value;

    $.ajax({
        url: "status_update.php",
        type: "POST",
        data : dataString,
        success: function(data){
            if(data == 'true'){
                if(option == 1){
                    toastr.success('Category Status Successfully Activated!');    
                }else if(option == 0){
                    toastr.success('Category Status Successfully Deactivated!');    
                }
                
            }else{
                toastr.success('Category Status Successfully Change!');
            }
        }
    });
})

$(document).on('change','.resolved',function(){
    if(this.checked){
        option = 1;
    } else {
        option = 0;
    }

    var type = 7;
    var id = $(this).data('id');
    var value = $(this).data('value');

    var dataString = 'id=' + id + '&option=' + option + '&type=' + type + '&value=' + value;

    $.ajax({
        url: "status_update.php",
        type: "POST",
        data : dataString,
        success: function(data){

            if(data == 'true'){
                if(option == 1){
                    toastr.success('Customer Complaint Has Been Resolved!');
                }else if(option == 0){
                    toastr.success('Customer Complaint Has Been Activated!');
                }
                
            }else{
                toastr.success('Customer Complaint Successfully Change!');
            }
        }
    });
})
$("#updateProfileForm").validate({
    errorElement : 'span',
    rules: {
        name: {
            required: true,
        },
        phone_number: {
            required: true,
            minlength: 10,
            maxlength: 10
        }
    },
    messages: {
        name: {
            required: "Please enter name",
        },
        phone_number: {
            required: 'Enter mobile number',
            minlength:"Enter minimum 10 digit mobile number",
            maxlength:"Enter minimum 10 digit mobile number",
        },
    }
});

$("#registerForm").validate({
    errorElement : 'span',
    rules: {
        name: {
            required: true,
        },
        email: {
            required: true,
            email:true,
            remote:{
                url: "check-mail",
                type: "post",
                data:{
                    email : function() {
                        return $('.email').val()
                    }
                }
            }
        },
        psw: {
            required: true,
        },
        confirm_password: {
            required: true,
            equalTo : "#psw",
        },
        phone_number: {
            required: true,
            minlength: 10,
            maxlength: 10
        }

    },
    messages: {
        name: {
            required: "Please enter name",
        },
        email: {
            required: "Please enter email id",
            remote:'Email already registed!'
        },
        psw:{
            required: "Please enter password",            
        },
        confirm_password:{
            required: "Please enter password",            
            equalTo : "Password not match",
        },
        phone_number: {
            required: 'Enter mobile number',
            minlength:"Enter minimum 10 digit mobile number",
            maxlength:"Enter minimum 10 digit mobile number",
        },
    }
});

$("#loginForm").validate({
    errorElement : 'span',
    rules: {
        email: {
            required: true,
            email:true,
            remote:{
                url: "check-credentials",
                type: "post",
                data:{
                    email : function() {
                        return $('.email').val()
                    }
                }
            }
        },
        psw: {
            required: true,
            remote:{
                url: "check-credentials",
                type: "post",
                data:{
                    email : function() {
                        return $('.email').val()
                    },
                    psw : function() {
                        return $('.psw').val()
                    },
                }
            }
        }

    },
    messages: {
        email: {
            required: "Please enter email id",
            remote:'Email id is not registered!'
        },
        psw:{
            required: "Please enter password",
            remote:'credentials not match or active your email id'
        }
    }

});



